var path       = require('path');
var express    = require('express');
var bodyParser = require('body-parser');
var cors       = require('cors');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cors());

app.use(express.static(path.join(__dirname, "../public")));

app.use(function (req, resp) {
    resp.status(404);
    resp.send("Error File Not Found");
});

app.listen('3000', function() {
    console.log("Server is running at http://localhost:3000");
});
